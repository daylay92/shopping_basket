import { Controller, Get, Query } from '@nestjs/common';
import { PaginatedResponse } from '../types';
import { GetProductsDto } from './product.dto';
import { Product } from './product.schema';
import { ProductService } from './product.service';

@Controller('/api/v1/products')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get()
  getProducts(
    @Query() dto: GetProductsDto,
  ): Promise<PaginatedResponse<Partial<Product>>> {
    return this.productService.getProducts(dto);
  }
}
