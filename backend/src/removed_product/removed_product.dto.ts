import {
  IsOptional,
  IsNumberString,
  IsString,
  ArrayNotEmpty,
  ArrayUnique,
} from 'class-validator';

export class CreateRemovedProductDto {
  @IsString({
    each: true,
  })
  @ArrayNotEmpty()
  @ArrayUnique()
  productIds: string[];
}

export class GetRemovedProductsDto {
  @IsOptional()
  @IsNumberString()
  pageNumber?: string;

  @IsOptional()
  @IsNumberString()
  pageSize?: string;

  @IsOptional()
  @IsString()
  search?: string;
}

export class FetchRemovedProductsResponse {
  id: string;
  name: string;
  frequency: number;
}
