import { Body, Controller, Get, HttpCode, Post, Query } from '@nestjs/common';
import { PaginatedResponse } from '../types';
import {
  GetRemovedProductsDto,
  CreateRemovedProductDto,
  FetchRemovedProductsResponse,
} from './removed_product.dto';
import { RemovedProduct } from './removed_product.schema';
import { RemovedProductService } from './removed_product.service';

@Controller('/api/v1/removed-products')
export class RemovedProductController {
  constructor(private readonly removedProductService: RemovedProductService) {}

  @Get()
  async getRemovedProducts(
    @Query() dto: GetRemovedProductsDto,
  ): Promise<PaginatedResponse<FetchRemovedProductsResponse>> {
    return this.removedProductService.getRemovedProducts(dto);
  }

  @Post()
  @HttpCode(201)
  async addRemovedProducts(
    @Body() dto: CreateRemovedProductDto,
  ): Promise<RemovedProduct[]> {
    return this.removedProductService.addRemovedProduct(dto);
  }
}
